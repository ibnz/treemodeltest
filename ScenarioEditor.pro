#-------------------------------------------------
#
# Project created by QtCreator 2015-01-21T15:59:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ScenarioEditor
TEMPLATE = app


SOURCES += main.cpp \
    scenarioeditor.cpp \
    modelscenariopoint.cpp \
    pointscenario.cpp

HEADERS  += \
    scenarioeditor.h \
    modelscenariopoint.h \
    pointscenario.h

FORMS    += \
    scenarioeditor.ui
