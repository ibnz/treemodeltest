#include "scenarioeditor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ScenarioEditor se;
    se.show();

    return a.exec();
}
