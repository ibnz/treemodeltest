#include "modelscenariopoint.h"
#include "tcpserver.h"
#include <QDebug>

#include <QJsonDocument>

ModelScenarioPoint::ModelScenarioPoint(QObject *parent):
    QAbstractItemModel(parent)
{
    m_rootPoint = new PointScenario();
    PointScenario *p1 = new PointScenario(m_rootPoint);
    p1->caption = "1";

    PointScenario *p2 = new PointScenario(m_rootPoint);
    p2->caption = "2";

    PointScenario *p21 = new PointScenario(p2);
    p21->caption = "21";

    PointScenario *p22 = new PointScenario(p2);
    p22->caption = "22";

    PointScenario *p23 = new PointScenario(p2);
    p23->caption = "23";

    PointScenario *p3 = new PointScenario(m_rootPoint);
    p3->caption = "3";
}


ModelScenarioPoint::~ModelScenarioPoint()
{
    delete m_rootPoint;
}

bool ModelScenarioPoint::removeRow(int row, const QModelIndex &i)
{
    if (row < 0) return false;
    PointScenario *parent = getPoint(i);
    if (row < parent->childList.count()){
        beginRemoveRows(i, row, row);{
            delete parent->childList[row];
        }endRemoveRows();
        return true;
    }else return false;
}

void ModelScenarioPoint::removeList(const QModelIndexList &list)
{
    QList<QPersistentModelIndex> persistentList;
    foreach (const QModelIndex &i, list) persistentList << QPersistentModelIndex(i);
    foreach (const QPersistentModelIndex &i, persistentList) {
        PointScenario *p = getPoint(i);
        if (p == m_rootPoint) continue;
        beginRemoveRows(i.parent(), i.row(), i.row());{
            delete p;
        }endRemoveRows();
    }
}

PointScenario *ModelScenarioPoint::getPoint(const QModelIndex &i)const
{
    if (i.isValid()) {
        PointScenario *item = static_cast<PointScenario *>(i.internalPointer());
        if (item) return item;
    }
    return m_rootPoint;
}


QModelIndex ModelScenarioPoint::index(int row, int column, const QModelIndex &parent) const
{
    PointScenario *parentItem = getPoint(parent);
    PointScenario *childItem = ((row >= 0)&&(row < parentItem->childList.count())) ? parentItem->childList.at(row) : 0;
    if (childItem) return createIndex(row, column, childItem);
    return QModelIndex();
}

int ModelScenarioPoint::rowCount(const QModelIndex &index) const {
    const PointScenario *point = static_cast<const PointScenario *>(index.internalPointer());
    if (!point) point = m_rootPoint;
    return point->childList.count();
}

QVariant ModelScenarioPoint::data(const QModelIndex &index, int role) const
{
    const PointScenario *point = getPoint(index);
    switch (role) {
    case Qt::DisplayRole:
        switch (index.column()){
            case CAPTION: return point->caption;
            case NAME: return point->name;
            case VALUE: return point->value;
            case TOLERANCE: return point->tolerance;
            default: break;
        }
        break;
    case Qt::EditRole:
        switch (index.column()){
            case CAPTION: return point->caption;
            case NAME: return point->name;
            case VALUE: return point->value;
            case TOLERANCE: return point->tolerance;
            default: break;
        }
        break;
    default:
        break;
    }
    return QVariant();
}

QVariant ModelScenarioPoint::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole){
        if (orientation == Qt::Horizontal){
            switch (section){
            case CAPTION: return "Действие";
            case NAME: return "Объект";
            case VALUE: return "Значение";
            case TOLERANCE: return "Допуск";
            default:return "N/A";
            }
        }
    }
    return QVariant();
}

Qt::ItemFlags ModelScenarioPoint::flags(const QModelIndex &i) const
{
    switch (i.column()) {
    case CAPTION:
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsEditable;
    case VALUE:
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsEditable;
    case TOLERANCE:
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsEditable;
    default:
        return Qt::ItemIsEnabled|Qt::ItemIsSelectable;
    }
}


bool ModelScenarioPoint::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole){
        PointScenario *p = getPoint(index);
        switch (index.column()) {
        case CAPTION:
            p->caption = value.toString();
            return true;
        case VALUE:
            p->value = value.toDouble();
            return true;
        case TOLERANCE:
            p->tolerance = qAbs(value.toDouble());
            return true;
        default:
            return false;
        }
    }
    return false;
}


bool ModelScenarioPoint::hasChildren(const QModelIndex &parent) const
{
    const PointScenario *point = getPoint(parent);
    return !point->childList.isEmpty();
}

QModelIndex ModelScenarioPoint::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    PointScenario *childItem = getPoint(index);
    PointScenario *parentItem = childItem->parent;

    if (parentItem == m_rootPoint)
        return QModelIndex();

    return createIndex(parentItem->childList.indexOf(childItem), 0, parentItem);
}
