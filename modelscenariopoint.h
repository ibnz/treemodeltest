#ifndef MODELSCENARIOPOINT_H
#define MODELSCENARIOPOINT_H
#include <QAbstractItemModel>
#include "pointscenario.h"

class ModelScenarioPoint : public QAbstractItemModel
{
public:
    enum COLUMNS{CAPTION, NAME, VALUE, TOLERANCE, COLUMNCOUNT};
    ModelScenarioPoint(QObject *parent = 0);
    ~ModelScenarioPoint();
    bool removeRow(int row, const QModelIndex &i);
    bool removePoint(PointScenario *p);
    void removeList(const QModelIndexList& list);
private:
    PointScenario *m_rootPoint;
    PointScenario *getPoint(const QModelIndex &i) const;

// QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &index) const;
    int columnCount(const QModelIndex &) const {return COLUMNCOUNT;}
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &i) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual bool hasChildren(const QModelIndex &parent) const;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const;
    virtual QModelIndex parent(const QModelIndex &index) const;
};

#endif // MODELSCENARIOPOINT_H
