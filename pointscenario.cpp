#include "pointscenario.h"
#include <QJsonArray>
#include <QDebug>

PointScenario::PointScenario(PointScenario *parentPoint):
    parent(parentPoint),
    value(0),
    tolerance(0)
{
    if (parent) parent->childList << this;
}

PointScenario::~PointScenario()
{
    qDeleteAll(childList);
    if (parent) parent->childList.removeOne(this);
}
