#ifndef POINTSCENARIO_H
#define POINTSCENARIO_H

#include <QList>
#include <QString>

struct PointScenario{
    PointScenario(PointScenario *parentPoint = 0);
    ~PointScenario();
    QString name;
    QString caption;
    qreal value;
    qreal tolerance;
    PointScenario *parent;
    QList<PointScenario *> childList;
};

#endif // POINTSCENARIO_H
