#include "scenarioeditor.h"
#include "ui_scenarioeditor.h"
#include "modelscenariopoint.h"
#include <QAction>
#include <QMenu>

ScenarioEditor::ScenarioEditor(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ScenarioEditor)
{
    ui->setupUi(this);
    m_model = new ModelScenarioPoint(this);
    ui->view->setModel(m_model);
    m_removeAction = new QAction("Удалить", this);
    connect(m_removeAction, &QAction::triggered, this, &ScenarioEditor::onRemoveActionTriggered);
}

ScenarioEditor::~ScenarioEditor()
{
    delete ui;
}

void ScenarioEditor::onRemoveActionTriggered()
{
    auto selectedIndexes = ui->view->selectionModel()->selectedRows();
    m_model->removeList(selectedIndexes);
//    ui->view->selectionModel()->clear();
}

void ScenarioEditor::on_view_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    menu.addActions(QList<QAction *>() << m_removeAction/* << m_addAction*/);
    menu.exec(ui->view->mapToGlobal(pos));
}
