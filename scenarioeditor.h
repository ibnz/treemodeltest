#ifndef SCENARIOEDITOR_H
#define SCENARIOEDITOR_H

#include <QtWidgets/QFrame>
class QAction;
class ModelScenarioPoint;

namespace Ui {
class ScenarioEditor;
}

class ScenarioEditor : public QFrame
{
    Q_OBJECT

public:
    explicit ScenarioEditor(QWidget *parent = 0);
    ~ScenarioEditor();

private:
    Ui::ScenarioEditor *ui;
    ModelScenarioPoint *m_model;
    QAction *m_removeAction;
private slots:
    void onRemoveActionTriggered();
    void on_view_customContextMenuRequested(const QPoint &pos);
};

#endif // SCENARIOEDITOR_H
